<?php jgiraldo_options_hanlder() ?>
<style>
	.jgiraldo-options{
		width: 60%;
		padding: 1%;
		float: left;
	}
	.jgiraldo-options label{
		width:20%;
		float: left;
		font-weight: bold;
	}
	.jgiraldo-options textarea{
		height:200px;
	}
	.jgiraldo-options textarea,
	.jgiraldo-options input{
		float:right;
		width:70%;
	}
	.jgiraldo-options p{
		margin:2%;
		width: 100%;
		float: left;
	}
</style>

<h1> Jgiraldo Theme Panel</h1><br/>
<div class="jgiraldo-options">
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<p><label> Headline </label>
		<textarea  name="jgiraldo_headline"><?php echo get_option("jgiraldo_headline") ?></textarea></p>

		<p><label> Link Headline </label> 
		<input type='text'  name="jgiraldo_link" value="<?php echo get_option("jgiraldo_link") ?>"/></p>
		<p><label> Back Header </label>
		<input type='text'  name="jgiraldo_back_header" value="<?php echo get_option("jgiraldo_back_header") ?>"/></p>
		<p><label> Color </label>
		<input type='text'  name="jgiraldo_color" value="<?php echo get_option("jgiraldo_color") ?>" /></p>
		<p><label> BackColor </label>
		<input type='text'  name="jgiraldo_back_color" value="<?php echo get_option("jgiraldo_back_color") ?>" /></p>
		
		<p><label> Logo </label>
		<input type='text'  name="jgiraldo_logo_image" value="<?php echo get_option("jgiraldo_logo_image") ?>" /></p>

		<p><label> Scroll Button </label>
		<input type='text'  name="jgiraldo_scroll_button" value="<?php echo get_option("jgiraldo_scroll_button") ?>" /></p>

		<p><label> Menu Button </label>
		<input type='text'  name="jgiraldo_menu_button" value="<?php echo get_option("jgiraldo_menu_button") ?>" /></p>

		<p><label> Search Button </label>
		<input type='text'  name="jgiraldo_search_button" value="<?php echo get_option("jgiraldo_search_button") ?>" /></p>

		<p><label> Info Button </label>
		<input type='text'  name="jgiraldo_info_button" value="<?php echo get_option("jgiraldo_info_button") ?>" /></p>

		<p><label> Close Button </label>
		<input type='text'  name="jgiraldo_close_button" value="<?php echo get_option("jgiraldo_close_button") ?>" /></p>

		<p><label> Good Attribute </label>
		<input type='text'  name="jgiraldo_good_att" value="<?php echo get_option("jgiraldo_good_att") ?>" /></p>
	 
		<p><label> Bad Attribute </label>
		<input type='text'  name="jgiraldo_bad_att" value="<?php echo get_option("jgiraldo_bad_att") ?>" /></p>

		<p><label> Week Poll </label>
		<input type='text'  name="jgiraldo_week_poll" value="<?php echo get_option("jgiraldo_week_poll") ?>" /></p>

		<p><label> Background Intro Image </label>
		<input type='text'  name="jgiraldo_back_intro_image" value="<?php echo get_option("jgiraldo_back_intro_image") ?>" /></p>
 
		<p><label> Background Intro Color </label>
		<input type='text'  name="jgiraldo_back_intro_color" value="<?php echo get_option("jgiraldo_back_intro_color") ?>" /></p>

		<p><input type="submit" value="Save" name="save_jgiraldo_options"/></p>
	</form> 
</div>
