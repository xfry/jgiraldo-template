<?php jgiraldo_options_hanlder() ?>
<style>
	.jgiraldo-options{
		width: 60%;
		padding: 1%;
		float: left;
	}
	.jgiraldo-options label{
		width:20%;
		float: left;
		font-weight: bold;
	}
	.jgiraldo-options textarea{
		height:200px;
	}
	.jgiraldo-options textarea,
	.jgiraldo-options input{
		float:right;
		width:70%;
	}
	.jgiraldo-options p{
		margin:2%;
		width: 100%;
		float: left;
	}
</style>
<h1> JGiraldo Social Options Panel</h1><br/>
<div class="jgiraldo-options">
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
	<h3>Social icons</h3>

		<p><label>Icon social 1 </label>
		<input type='text'  name="jgiraldo_icon_social_1" value="<?php echo get_option("jgiraldo_icon_social_1") ?>" /></p>

		<p><label>Icon social 2 </label>
		<input type='text'  name="jgiraldo_icon_social_2" value="<?php echo get_option("jgiraldo_icon_social_2") ?>" /></p>

		<p><label>Icon social 3 </label>
		<input type='text'  name="jgiraldo_icon_social_3" value="<?php echo get_option("jgiraldo_icon_social_3") ?>" /></p>

		<p><label>Icon social 4 </label>
		<input type='text'  name="jgiraldo_icon_social_4" value="<?php echo get_option("jgiraldo_icon_social_4") ?>" /></p>

		<p><label>Icon social 5 </label>
		<input type='text'  name="jgiraldo_icon_social_5" value="<?php echo get_option("jgiraldo_icon_social_5") ?>" /></p>

		<p><label>Icon social 6 </label>
		<input type='text'  name="jgiraldo_icon_social_6" value="<?php echo get_option("jgiraldo_icon_social_6") ?>" /></p>

		<p><label>Icon social 7 </label>
		<input type='text'  name="jgiraldo_icon_social_7" value="<?php echo get_option("jgiraldo_icon_social_7") ?>" /></p>

		<p><label>Icon social 8 </label>
		<input type='text'  name="jgiraldo_icon_social_8" value="<?php echo get_option("jgiraldo_icon_social_8") ?>" /></p>

	<h3>Social links</h3>

		<p><label> Link social 1 </label>
		<input type='text'  name="jgiraldo_links_social_1" value="<?php echo get_option("jgiraldo_links_social_1") ?>" /></p>

		<p><label> Link social 2 </label>
		<input type='text'  name="jgiraldo_links_social_2" value="<?php echo get_option("jgiraldo_links_social_2") ?>" /></p>

		<p><label> Link social 3 </label>
		<input type='text'  name="jgiraldo_links_social_3" value="<?php echo get_option("jgiraldo_links_social_3") ?>" /></p>

		<p><label> Link social 4 </label>
		<input type='text'  name="jgiraldo_links_social_4" value="<?php echo get_option("jgiraldo_links_social_4") ?>" /></p>

		<p><label> Link social 5 </label>
		<input type='text'  name="jgiraldo_links_social_5" value="<?php echo get_option("jgiraldo_links_social_5") ?>" /></p>

		<p><label> Link social 6 </label>
		<input type='text'  name="jgiraldo_links_social_6" value="<?php echo get_option("jgiraldo_links_social_6") ?>" /></p>

		<p><label> Link social 7 </label>
		<input type='text'  name="jgiraldo_links_social_7" value="<?php echo get_option("jgiraldo_links_social_7") ?>" /></p>

		<p><label> Link social 8 </label>
		<input type='text'  name="jgiraldo_links_social_8" value="<?php echo get_option("jgiraldo_links_social_8") ?>" /></p>

		<p><input type="submit" value="Save" name="save_jgiraldo_options"/></p>
	</form> 
</div>