<?php 
add_action('admin_menu', 'jgiraldo_theme_panel');
add_action("init", "jgiraldo_register_theme_options");

function jgiraldo_register_theme_options(){
	if(!get_option("jgiraldo_headline")){
		add_option("jgiraldo_headline", "JGiraldo Reporta es el reporte de Juan Carlos Giraldo,
      editor y periodista de moda, estilo y buen vivir ");
	}
	if(!get_option("jgiraldo_link")){
		add_option("jgiraldo_link", "#");
	}
	if(!get_option("jgiraldo_back_header")){
		add_option("jgiraldo_back_header", "#1A1A1A");
	}
	if(!get_option("jgiraldo_color")){
		add_option("jgiraldo_color", "#69F269");
	}
	if(!get_option("jgiraldo_logo_image")){
		add_option("jgiraldo_logo_image", get_bloginfo("template_url") . "/images/logo-footer.png");
	}
	if(!get_option("jgiraldo_scroll_button")){
		add_option("jgiraldo_scroll_button", get_bloginfo("template_url") . "/images/icon-go-up-medium.png");
	}
	if(!get_option("jgiraldo_menu_button")){
		add_option("jgiraldo_menu_button", get_bloginfo("template_url") . "/images/icon-menu-medium.png");
	}
	if(!get_option("jgiraldo_search_button")){
		add_option("jgiraldo_search_button", get_bloginfo("template_url") . "/images/search-medium.png");
	}
	if(!get_option("jgiraldo_info_button")){
		add_option("jgiraldo_info_button", get_bloginfo("template_url") . "/images/logo-info.png");
	}
	if(!get_option("jgiraldo_close_button")){
		add_option("jgiraldo_close_button", get_bloginfo("template_url") . "/images/icon-close.png");
	}
	if(!get_option("jgiraldo_good_att")){
		add_option("jgiraldo_good_att", get_bloginfo("template_url") . "/images/impar-icon.png");
	}
	if(!get_option("jgiraldo_bad_att")){
		add_option("jgiraldo_bad_att", get_bloginfo("template_url") . "/images/impar-icon.png");
	}
	if(!get_option("jgiraldo_week_poll")){
		add_option("jgiraldo_week_poll", 1);
	}

	
}
function jgiraldo_theme_panel() {
	
	add_menu_page('Jgiraldo Options', 'jGiraldo Options', 'edit_theme_options', 'jgiraldo-options', 'jgiraldo_panel_render');
	add_submenu_page( 'jgiraldo-options', 'jGiraldo Menu Options', 'jGiraldo Menu Options', 'edit_theme_options', 'jgiraldo-menu-options', 'jgiraldo_menu_options_render' );
	add_submenu_page( 'jgiraldo-options', 'jGiraldo social Options', 'jGiraldo social Options', 'edit_theme_options', 'jgiraldo-social-options', 'jgiraldo_social_options_render' );
}


function jgiraldo_panel_render(){ 
	include_once("theme-form.php");
}
function jgiraldo_menu_options_render(){ 
	include_once("theme-menu-form.php");
}
function jgiraldo_social_options_render(){ 
	include_once("theme-social-form.php");
}
function jgiraldo_options_hanlder(){
	if(!isset($_POST))
		exit();
	if(isset($_POST["save_jgiraldo_options"])){
		foreach ($_POST as $key => $value) {
			update_option($key,$value);
		}
	}
}

function jgiraldo_custom_styles(){
	echo "
		<style>
			div.header {
				background-color: ".get_option("jgiraldo_back_header" )." 
			}
			a.menu-content-left, a.menu-content-right,
			li.filtro-item div.radio-btn, div.reporte,div.side-menu-conten{
				background-color: ".get_option("jgiraldo_color")." !important
			}
			.wp-polls input.Buttons, 
			li.filtro-item div.radio-btn, 
			div.cloud-tags .tags-menu li{
				border-color: ".get_option("jgiraldo_color")." !important
			}
			div.category-card div.detail-section h2 a:hover,
			div.footer-components ul.menu-items li a:hover,
			.title-report a, span.back-home, 
			div.links-items a h2:hover, 
			li.filtro-item span:hover, div.search-result h3, div.panel1-right-container h1, .adsdescription a, .titulo a, div.intro-preposition em {
				color: ".get_option("jgiraldo_color")." !important
			}
			html{background-color:rgba(".get_option("jgiraldo_back_color").")}
			
			span.logo-icon,div.footer-components span.logo-footer{
				background-image: url('".get_option("jgiraldo_logo_image")."') 
			}
			div.footer-components span.icon-goup,a span.go-up-arrow{
				background-image: url('".get_option("jgiraldo_scroll_button")."');
			}
			span.menu-icon{
				background-image: url('".get_option("jgiraldo_menu_button")."')
			}
			span.search-icon,div.serch-items .icon-search{
				background-image: url('".get_option("jgiraldo_search_button")."');
				background-size:cover;
			}
			div.panel1-left-container .info-icon{
				background-image: url('".get_option("jgiraldo_info_button")."')
			}
			.mal.category-card .img-icon-par{
				background-image: url('".get_option("jgiraldo_bad_att")."')
			}
			.bien.category-card .img-icon-par{
				background-image: url('".get_option("jgiraldo_good_att")."')
			}
			.intro-preposition{
				background: url('".get_option("jgiraldo_back_intro_image")."') no-repeat center center;
				background-size: cover;
			}
			.back-intro{
				background-color:rgba(".get_option("jgiraldo_back_intro_color").")
			}
		</style>";
}

add_action("wp_head", "jgiraldo_custom_styles");