<?php jgiraldo_options_hanlder() ?>
<style>
	.jgiraldo-options{
		width: 60%;
		padding: 1%;
		float: left;
	}
	.jgiraldo-options label{
		width:20%;
		float: left;
		font-weight: bold;
	}
	.jgiraldo-options textarea{
		height:200px;
	}
	.jgiraldo-options textarea,
	.jgiraldo-options input{
		float:right;
		width:70%;
	}
	.jgiraldo-options p{
		margin:2%;
		width: 100%;
		float: left;
	}
</style>
<h1> JGiraldo  Menu Options Panel</h1><br/>
<div class="jgiraldo-options">
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
	<h3>Menu Colors</h3>

		<p><label>  Color Menu 1 </label>
		<input type='text'  name="jgiraldo_color_menu_1" value="<?php echo get_option("jgiraldo_color_menu_1") ?>" /></p>

		<p><label>  Color Menu 2 </label>
		<input type='text'  name="jgiraldo_color_menu_2" value="<?php echo get_option("jgiraldo_color_menu_2") ?>" /></p>

		<p><label>  Color Menu 3 </label>
		<input type='text'  name="jgiraldo_color_menu_3" value="<?php echo get_option("jgiraldo_color_menu_3") ?>" /></p>

		<p><label>  Color Menu 4 </label>
		<input type='text'  name="jgiraldo_color_menu_4" value="<?php echo get_option("jgiraldo_color_menu_4") ?>" /></p>

		<p><label>  Color Menu 5 </label>
		<input type='text'  name="jgiraldo_color_menu_5" value="<?php echo get_option("jgiraldo_color_menu_5") ?>" /></p>

		<p><label>  Color Menu 6 </label>
		<input type='text'  name="jgiraldo_color_menu_6" value="<?php echo get_option("jgiraldo_color_menu_6") ?>" /></p>


		<p><input type="submit" value="Save" name="save_jgiraldo_options"/></p>
	</form> 
</div>