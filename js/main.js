$ = jQuery.noConflict();
$(document).ready(function (argument) {
  //$(".tag .report-category").mixItUp();
  var $popupText = $(".adsdtext"),
      $panelImage = $(".panel1-left-container"),
      $panel1 = $(".panel1"),
      $iconInfo = $(".icon-info-container"),
      $iconClose = $(".icon-close"),
      $textPopup = $(".adstext-description"),
      $social2 = $(".panel1-left-side > .social2");
      $socialIcon = $(".adsdtext > .social2");
      $headerReporte = $(".header-reporte");

  $iconInfo.on("click", this, function(){
    alejo = $(this);
    var $posPanel = $panel1.position();
    
    if ( $posPanel != $panel1.position) {
      $popupText.css({
        "height": ($panel1.height())
      });
      
      
      $iconClose.css({ 
        "top": (parseFloat($(this).css("margin-top")) + $(this).parents(".panel1-left-side").find(".header-reporte").height() + parseFloat($(this).parents(".panel1-left-side").css("margin-top")) + 15  ),
        
      });
      $iconClose.find("img").width(parseFloat($(this).css("width") ) );

      $textPopup.css({
        "margin-top": ($(this).position().top + $(this).height() ),
      });

      $socialIcon.css({
        "position": "relative",
      });
      $iconInfo.css("z-index", "1");
    }

    
    if(window.innerWidth < 1024) {
      var $popText = $(this).parents(".panel1-left-side").next(".adsdtext");
      $popText.height($(this).parents(".panel1").height());
      $popText.show(800);
      $iconClose.on( 'click', function() {
        $popText.hide(800);
        $iconInfo.css("z-index", "9");
      });
    }
  });
  
  /*Go up handler*/
  $(".go-up-arrow,.icon-goup").click(function(e){
    e.preventDefault();
    $("body,html").animate({scrollTop:0}, '500', 'swing');
  });
  
  //checkedRadio();
  toogleMenu();
  
  /*on scroll change icon*/  
  window.onscroll = function () {
    var $logoIcon = $(".logo-icon"),
        $goUpIcon = $('a > span.go-up-arrow');
    
    if (document.body.scrollTop > 100) {
      $logoIcon.hide();
      $goUpIcon.slideDown('slow').fadeIn('fast');
      $goUpIcon.css("display","inline-block");
    } else  {
      $goUpIcon.hide();
      $goUpIcon.css("display","none");
      $logoIcon.slideDown('slow');
    }
  }
});

var toogleMenu = function() {
  var $menuIcon = $("a > .menu-icon"),
      $menuContent = $(".menu"),
      $socialClose = $(".social-icon-close");

  $menuIcon.on('click', function (e) {
    e.preventDefault();
    $menuContent.slideToggle('fast');
    $(".header-nav-items").hide();
    $("div.header").css("background-color","#1a1a1a");
  });
  $(".search-icon").click(function(){
    $menuIcon.click();
    $("div.serch-items input[type='text']").focus(); 
  })
  $socialClose.on('click', function (argument) {
    $(".header-nav-items").slideToggle('fast');
    $("div.header").css("background-color","");
    $menuContent.hide();
  });
}

/*var checkedRadio = function () {
  var $radioBtnBien = $("#filtro-bien"),
      $radioBtnMal = $("#filtro-mal"),
      $radioBtnHombres = $("#filtro-hombres"),
      $radioBtnMujeres = $("#filtro-mujeres");

  $radioBtnBien.on('click', function() {
    if(! $radioBtnBien.hasClass('selected')) {
      $radioBtnBien.addClass("selected")
    } else {
      $radioBtnBien.removeClass("selected");
    }
  });

  $radioBtnMal.on('click', function() {
    if(! $radioBtnMal.hasClass('selected')) {
      $radioBtnMal.addClass("selected")
    } else {
      $radioBtnMal.removeClass("selected");
    }
  });

  $radioBtnHombres.on('click', function() {
    if(! $radioBtnHombres.hasClass('selected')) {
      $radioBtnHombres.addClass("selected")
    } else {
      $radioBtnHombres.removeClass("selected");
    }
  });

  $radioBtnMujeres.on('click', function() {
    if(! $radioBtnMujeres.hasClass('selected')) {
      $radioBtnMujeres.addClass("selected")
    } else {
      $radioBtnMujeres.removeClass("selected");
    }
  });
};*/