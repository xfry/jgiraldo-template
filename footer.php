
  <!-- footer -->
  <div class="footer">
    <div class="footer-components col-14">
      <a href="<?php bloginfo("url") ?>" title="<?php bloginfo("name")?>"><span class="logo-footer col-1"></span></a>
      <?php jgiraldo_bottom_menu() ?>
      <!--<ul class="menu-items col-5">
        <li class="item"><a href="#">Nosotros · </a></li>
        <li class="item"><a href="#">Contacto · </a></li>
        <li class="item"><a href="#">Pauta · </a></li>
        <li class="item"><a href="#">Terminos y Privacidad</a></li><br/>
        <li class="item"><p>© 2015 JGiraldoReporta · Todos los derechos reservados</p></li>
      </ul>->
      
      <!-- social section left -->
      <div class="section-go-back col-3">
        <span class="icon-goup"></span>
      </div>
      <div class="social-section col-5">
        <a href="http://facebook.com/jgiraldoreporta"><img class="social-icon-fb" src="<?php bloginfo("template_url") ?>/images/facebook-with-circle-light.svg" width="22px" alt="facebook icon" /></a>
        <a href="http://twitter.com/jgiraldoreporta"><img class="social-icon-t" src="<?php bloginfo("template_url") ?>/images/twitter-with-circle-light.svg" width="22px" alt="facebook icon" /></a>
        <a href="http://instagram.com/jgiraldoreporta"><img class="social-icon-msg" src="<?php bloginfo("template_url") ?>/images/instagram-with-circle-light.svg" width="22px" alt="facebook icon" /></a>
        <a href="http://pinterest.com/jgiraldoreporta"><img class="social-icon-pinterest" src="<?php bloginfo("template_url") ?>/images/pinterest-with-circle-light.svg" width="22px" alt="facebook icon" /></a>
        <a href="https://www.youtube.com/jgiraldoreporta"><img class="social-icon-yoututbe" src="<?php bloginfo("template_url") ?>/images/youtube-with-circle-light.svg" width="22px" alt="facebook icon" /></a>
      </div>
      <a href="http://oficiosvarios.co" title="OFICIOSVARIOS," target="_blank"><span class="social-icon-oficios-varios"></span></a>
    </div>
  </div>
  <!-- end footer -->
</div><!-- end wrapper --> 
  <?php wp_footer()?>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48045941-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>