<?php get_header() ?>

  <div class="reporte-unico">
    <!-- heder reporte-unico -->
    <div class="reporte-unico-container">
      <div class="header-reporte-unico">
        <h1 class="titulo">Error en el servidor</h1>
        <div class="social-icons-hr"></div>
      </div><!-- end header reporte -->
      <!-- report social section -->
      
    </div><!-- end report container -->
  </div><!-- end reporte -->
  
  <div class="page-container col-14"> 
      <h2> Oops parece que ocurrió un error al intentar servir tu pagina, intenta lo de nuevo.</h2>
  </div>
  
<?php get_footer() ?>