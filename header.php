<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Permalink site</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url")?>/stylesheet/reset.css">
  <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url")?>/stylesheet/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <meta name="p:domain_verify" content="0eb3b36fd2888863603ea1387f12b9ad"/>
  <script>var thing_imgs = 0;</script>
  <?php wp_head() ?>
</head>
<body <?php body_class() ?> >
  <div class="header">
      <div class="new-header-nav-items">
        <a class="a-ulmenu-menu-icon" href="#"><span class="ulmenu-menu-icon"><i class="fa fa-bars"></i></span></a>
      </div>
      
      <div class="menu">
        <ul class="ulmenu">
          <?php wp_nav_menu( array( 'theme_location' => 'new_menu', 'container' => '', 'items_wrap' => '%3$s' ) ) ?>
          <li class="col-4">
            <form method="get" action="/">
            <input name="s" type="search" placeholder="Buscar">
            
            <button type="submit"><i class="fa fa-search"></i></button>
            </form>
          </li>
          <li class="col-3">
            <ul class="menu-social">
              <?php for( $i = 1; $i < 9; $i++ ) : ?>
                <?php if( get_option( 'jgiraldo_links_social_' . $i ) != "" && get_option( 'jgiraldo_icon_social_' . $i ) != "" ) : ?>
                  <li><a target="_blank" href="<?php echo get_option( 'jgiraldo_links_social_' . $i) ?>"><i class="fa fa-<?php echo get_option( 'jgiraldo_icon_social_' . $i) ?>"></i></a></li>
                <?php endif; ?>  
              <?php endfor; ?>
            </ul>
          </li>
          <li class="col-2"><a href="/contacto/">Contacto</a></li>
          <li class="li-close"><span class="social-icon-close second-icon-close"><i class="fa fa-times-circle"></i></span></li>
        </ul>
      </div>
    </div>
    <script type="text/javascript">
    $ = jQuery.noConflict();
    $(document).ready(function (argument) {
      <?php if( is_home() || is_front_page() ) : ?>
        $( '.menu' ).css( "border-bottom-color", '<?php echo get_option( 'jgiraldo_color_menu_1' ) ?>' );
      <?php elseif( is_search() ) : ?>
        $( '.menu' ).css( "border-bottom-color", '<?php echo get_option( 'jgiraldo_color_menu_4' ) ?>' );
      <?php elseif( is_page('contacto') ) : ?>
        $( '.menu' ).css( "border-bottom-color", '<?php echo get_option( 'jgiraldo_color_menu_6' ) ?>' );
      <?php elseif(is_singular()) : ?>
        var menuColor = $('.current-post-ancestor').css("background-color");
        console.log(menuColor);
      <?php else: ?>
        var menuColor = $('.current-menu-item').css("background-color");
        console.log(menuColor);
      <?php endif; ?>
      if( typeof(menuColor) !== 'undefined' )
        $( '.menu' ).css( "border-bottom-color", menuColor );
      newToogleMenu();
      var menuColors = new Array();
      <?php for( $i = 1; $i < 7; $i++ ) : ?>
        menuColors.push("<?php echo get_option('jgiraldo_color_menu_' . $i) ?>");
      <?php endfor; ?>
      var colorCounter = 0;
      $(".ulmenu li").each(function(){
        if( $(this).parent(".ulmenu").length  ){
          $(this).css("background", menuColors[colorCounter]);
          colorCounter++;
        }
      });
    });

    var newToogleMenu = function() {
      var $menuIcon = $(".new-header-nav-items"),
          $menuContent = $(".menu"),
          $socialClose = $(".social-icon-close");

      $(".new-header-nav-items a").on('click', function (e) {
        e.preventDefault();
        $menuContent.slideDown(300);
        $menuIcon.hide();
      });
      
      $socialClose.on('click', function (e) {
        e.preventDefault();
        $menuIcon.slideDown(300);
        
        $menuContent.hide();
      });


    }
  </script>

  <div class="wrapper">