<?php 


wp_enqueue_script("mixitup","//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js", array("jquery") );
wp_enqueue_script("thinglink", "//cdn.thinglink.me/jse/embed.js");
wp_enqueue_style("mailchimp", "//cdn-images.mailchimp.com/embedcode/slim-081711.css");

if(strpos(get_bloginfo("url"), "jgiraldoreporta.com") > 1 )
  wp_enqueue_script("typekit", "//use.typekit.net/qie0ukc.js");
else
  wp_enqueue_script("typekit", "//use.typekit.net/yts8jds.js");

wp_enqueue_script("main-permalink", get_bloginfo("template_url") . "/js/main.js", array("jquery", "mixitup"), "1.0", TRUE );
add_theme_support("post-thumbnails");
add_action("wp_head" , "jgiraldo_send_to_friend_scripts", 1000 );
add_image_size("loop_image",578, 750, TRUE);
add_image_size("report_image",239, 375, TRUE);
add_image_size("report_image_2",357, 375, TRUE);
add_image_size("ads_image",375,375, TRUE);
add_image_size("historia_image",1280,375, TRUE);


register_nav_menus( array(
  'top_menu' => 'Top Menu',
  'footer_menu' => 'Footer Menu',
  'new_menu' => 'New Menu'
) );

function jgiraldo_main_loop( $iteration = 0, $ads_loop = FALSE ){
  global $post;
  if($post->post_type == "jgiraldo_reportes"){

    jgiraldo_roll_report($post);
  }else{
    if(($iteration % 2) == 0 ){
      $float = "left"; $float_left = "right";
    }else{
      $float = "right"; $float_left = "left"; 
    }

  	include(locate_template("content-loops/main-loop.php" ));
    jgiraldo_ads_loop($iteration, $ads_loop);
    

  }
}

function jgiraldo_search_loop( $iteration = 0, $container, $same_term = null, $tag_id = null ){
  global $wp_query;
  if(($iteration % 2) == 0 )     
    $even = true;
  else 
    $even = false;

  $total_posts = $wp_query->post_count;
  include(locate_template("content-loops/loop-search.php" ));
}

function jgiraldo_reporte_loop( $iteration = 0, $container ){
  global $wp_query;
  if(($iteration % 2) == 0 )     
    $even = true;
  else 
    $even = false;


  $total_posts = $wp_query->post_count;
  include(locate_template("content-loops/reporte-related-loop.php" ));
}

function jgiraldo_papeles_atributes_register_taxonomy(){
  register_taxonomy(
          'papel_atributos',
          array('post', 'jgiraldo_reportes'),
          array(
            'label' => __( 'Atributos' ),
            'rewrite' => array( 'slug' => 'papel-atributos' ),
            'hierarchical' => false,
          )
  );
}

add_action("init", "jgiraldo_papeles_atributes_register_taxonomy");



function jgiraldo_tag_cloud($number = 15, $title = null, $div = true){

  if(!isset($title))
    $title = "Quieres seguir explorando? Hay mil opciones";

  $tags = wp_tag_cloud( 'format=array&number='.$number.'&smallest=100&largest=100&unit=%&order=RAND' );
  
  if($div)
    echo '<div class="cloud-tags">';
  echo '<p class="cloud-tags-title">'.$title.'</p>
    <ul class="tags-menu col-10">';
  foreach ($tags as $tag){
    echo "<li class='tags-menu-item'>". $tag ."</a>";
  }
  echo '</ul>';
  if($div)
    '</div><!-- end cloudtags -->';
}

function jgiraldo_historias_de_marca() {
    $args = array(
      'public' => true,
      'label'  => 'Historias de Marca',
      'taxonomies' => array("post_tag"),
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions' ),
      'exclude_from_search' => TRUE
    );
    register_post_type( 'historias', $args );
}
add_action( 'init', 'jgiraldo_historias_de_marca' );

function jgiraldo_reportes() {
    $args = array(
      'public' => true,
      'label'  => 'Reportes',
      'taxonomies' => array("post_tag"),
      'rewrite' => array( 'slug' => 'reportes' ),
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions' )
    );
    register_post_type( 'jgiraldo_reportes', $args );
}
add_action( 'init', 'jgiraldo_reportes' );

function jgiraldo_ads() {
    $args = array(
      'public' => true,
      'label'  => 'Ads Jgiraldo',
      'taxonomies' => array("post_tag"),
      'supports' => array( 'title', 'editor', 'thumbnail',  'custom-fields', 'revisions' ),
      'exclude_from_search' => TRUE
    );
    register_post_type( 'ads_jgiraldo', $args );
}
add_action( 'init', 'jgiraldo_ads' );


function jgiraldo_post_tags(){
  global $post;
  echo get_the_tag_list('<ul class="tags-menu"><li class="tags-menu-item">','</li><li class="tags-menu-item">','</li></ul>');
}

function jgiraldo_post_attributes(){
  global $post;
  $terms = wp_get_post_terms($post->ID, "papel_atributos");
  foreach ($terms as $term){
    $output .= " " . strtolower($term->name) . " ";
  }
  echo $output;
}

function jgiraldo_loop_reportes(){
  global $post;
  $iteration = 0;

  $posts = explode ( ",",get_post_meta($post->ID, "reporte",true));

  foreach ($posts as $p) {

    query_posts("p=" . $p );
    
    if(have_posts()) : the_post();
      jgiraldo_main_loop($iteration,TRUE);
    endif;
    wp_reset_query();

    $iteration++;
  }
  

}

function webstripe_handler($atts) {
  extract(shortcode_atts(array('id' => '', ), $atts));
  wp_enqueue_script('webStripe', get_bloginfo('template_url') . '/js/webstripe.js',array("jquery"));
  wp_enqueue_style('webStripe', get_bloginfo('template_url') . '/stylesheet/webstripe.css');
  return '
    <script>
      jQuery(document).ready(function($){
        Webstripe.load(' . $id . ', "#webStripeContainer");
      });
    </script>
    <div id="webStripeContainer"></div>';   
}

add_shortcode('webstripe', 'webstripe_handler');



function jgiraldo_get_prev_post_link(){
  global $post;
  $same_term = $_GET["same_term"];
  $tag_id = $_GET["tag_id"];
  if( isset($same_term) && ($same_term == "category") ){
    //set cat query
    set_query_var("cat", $tag_id);
    $next_post = get_next_post(TRUE, "", $same_term  );
    echo get_permalink($next_post->ID ) . "?same_term=" . $same_term . "&tag_id=" . $tag_id;
  }elseif($same_term == "post_tag"){
    //set cat query
    set_query_var("tag_id", $tag_id);
    $next_post = get_next_post(TRUE, "", $same_term  );
    echo get_permalink($next_post->ID ) . "?same_term=" . $same_term . "&tag_id=" . $tag_id;
  }elseif($same_term == "search"){
    $next_post = get_next_post( TRUE );
    echo get_permalink($next_post->ID ) . "?same_term=" . $same_term . "&tag_id=" . $tag_id;
  }else{
    $next_post = get_next_post();
    echo get_permalink($next_post->ID );
  }
}

function jgiraldo_get_next_post_link(){ 
  global $post;
  $same_term = $_GET["same_term"];
  $tag_id = $_GET["tag_id"];
  if( isset($same_term) && ($same_term == "category") ){
    //set cat query
    set_query_var("cat", $tag_id);
    $next_post = get_previous_post(TRUE, "", $same_term  );
    echo get_permalink($next_post->ID ) . "?same_term=" . $same_term . "&tag_id=" . $tag_id;
  }elseif($same_term == "post_tag"){
    //set cat query
    set_query_var("tag_id", $tag_id);
    $next_post = get_previous_post(TRUE, "", $same_term  );
    echo get_permalink($next_post->ID ) . "?same_term=" . $same_term . "&tag_id=" . $tag_id;
  }elseif($same_term == "search"){
    $next_post = get_previous_post( TRUE );
    echo get_permalink($next_post->ID ) . "?same_term=" . $same_term . "&tag_id=" . $tag_id;
  }else{
    $next_post = get_previous_post();
    echo get_permalink($next_post->ID );
  }
}

function jgiraldo_related_posts_by_tag($rand = false){
     global $post;
     $tag = wp_get_post_tags($post->ID);
      $tag = $tag[rand(0,(count($tag) - 1))]->slug;
      $container = false; $iteration = 0; 
      if(!$rand)
        query_posts("tag=". $tag . "&posts_per_page=4&orderby=rand");
      else
        query_posts("posts_per_page=4&orderby=rand");

      if ( have_posts() ) : while ( have_posts() ) : the_post();
      if(($iteration % 4) == 0 )
        $container = !$container;

      jgiraldo_search_loop($iteration,$container);
      $iteration++;
      endwhile;endif; 
      wp_reset_query();
}

function jgiraldo_roll_report($report_post = null ){
     global $post, $report_permalink;
     
     $container = false; $iteration = 0; 
     
     if(!isset($report_post)){
        $last_post = get_posts("post_type=jgiraldo_reportes&posts_per_page=1");
     }else{
      $last_post[0] = $report_post;
     }
     $report_permalink = get_permalink($last_post[0]->ID);
     $post = $last_post[0];
     setup_postdata($post);
     echo '<!-- Reporte 01 section -->
           <div class="reporte">
              <!-- heder reporte -->
              <div class="reporte-container">
                <div class="header-reporte">
                  <h1 class="titulo col-4"><a href="'.$report_permalink.'">'.get_the_title($last_post[0]->ID) . '</a></h1>
                  <div class="icons-hr col-5"></div>';
      jgiraldo_post_tags();
      echo '</div><!-- end header reporte -->';

     wp_reset_postdata();
     $posts_to_loop = explode(",",get_post_meta($last_post[0]->ID, "reporte",true));
     
     $query_posts =  new WP_Query(array("post__in" => $posts_to_loop, "post__not_in" => get_option("sticky_posts")));
     
     $max_posts_to_iterate = intval ($query_posts->post_count / 4 ) * 4;

     if($max_posts_to_iterate > 8 ) $max_posts_to_iterate = 8;
     
     if ( $query_posts->have_posts() ) : while ( $query_posts->have_posts() && ($iteration < $max_posts_to_iterate) ) : $query_posts->the_post();
      if(($iteration % 4) == 0 )
        $container = !$container;
      
      jgiraldo_reporte_loop($iteration,$container);
      $iteration++;
      endwhile;endif; 
      wp_reset_postdata();

      echo '
            <!-- report social section -->
            <div class="social">
              <div class="icons-hr col-5"></div>'; 
      echo  jgiraldo_social_icons($last_post[0]);
              
      echo '</div><!-- end report social -->
          </div><!-- end report container -->
        </div><!-- end reporte -->';
}

function jgiraldo_tag_search_cat_loop(){
  if( is_tag() ) {
    $same_term = "?same_term=post_tag&tag_id=".get_query_var("tag_id");
  }elseif(is_search()){
    $same_term = "?same_term=search&tag_id=".get_query_var("s");
  }elseif(is_category()){
     $same_term = "?same_term=category&tag_id=".get_query_var("cat");
  }
  $container = false; $iteration = 0;
      if ( have_posts() ) : while ( have_posts() ) : the_post(); 
      if(($iteration % 4) == 0 )
        $container = !$container;

      jgiraldo_search_loop( $iteration, $container, $same_term);
      
      $iteration++;endwhile;
      else:
        echo '
          <div class="reporte error">
            <div class="reporte-wrap-serach">
              <div class="reporte-container">
              <h2> Oops parece que no hay papeles relacionados a tu busqueda.</h2>
              </div>
            </div>
          </div>  

    ';
      endif;
      /*global $wp_query;
      print_r($wp_query->query_vars);*/
}

function jgiraldo_historias_related($tag = null){
  global $post;

  $tag = wp_get_post_tags($post->ID);
  $tag = $tag[rand(0,(count($tag) - 1))]->slug;
  
  
  $args = array(
            'post_type' => 'historias',
            'posts_per_page' => 1,
            'tag' => $tag,
            'post__not_in' => array($post->ID)
        );
  $last_post = get_posts( $args );
  wp_reset_postdata();
  
  if(empty($last_post)){
    $args = array(
            'post_type' => 'historias',
            'posts_per_page' => 1,
            'post__not_in' => array($post->ID)
        );
    $last_post = get_posts( $args );
    wp_reset_postdata();
  }

  $last_post = $last_post[0];
    $attachment = wp_get_attachment_image_src(get_post_thumbnail_id($last_post->ID), "historia_image");
      echo '
            <style>
              div.search.historias div.search-result .back-intro:hover{background-color:rgba('.get_post_meta($last_post->ID,"back-color",true).',0.9)}
            </style>
            <div class="search historias">
              <div class="search-result" style="background: url('.$attachment[0].') no-repeat center center; background-size:cover;height:'.$attachment[2].'px">
                <div class="back-intro" style="background-color:rgba('.get_post_meta($last_post->ID,"back-color",true).',0.6)" >
                  <a href="'.get_permalink($last_post->ID).'"><h3>'.get_the_title($last_post->ID).'</h3></a>
                </div>
              </div>
            </div>';
    
  
}

function jgiraldo_social_icons($last_post = null){ 
  if(isset($last_post))
    $post = $last_post;
  else
    global $post;

  $post_thumbnail_id = get_post_thumbnail_id($post->ID);
  $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );

  echo '<div class="social-icons">
          <ul>
            <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.get_permalink($post->ID).'"><span class="fb-icon"></span></a></li>
            <li><a target="_blank"  href="http://twitter.com/share?url='.get_permalink($post->ID ).'&hashtags=JGiraldoReporta"><span class="tw-icon"></span></a></li>
            <li><a href="#"><span st_summary="'.get_the_excerpt( $post->ID ).'" st_image="'.$post_thumbnail_url.'" class="st_email" st_st_title="'.get_the_title($post->ID).'" st_url="'.get_permalink($post->ID).'"></span></a></li>
          </ul>
        </div><script>stButtons.locateElements();</script>';
}

function jgiraldo_send_to_friend_scripts(){
  echo '
      <script type="text/javascript">var switchTo5x=true;</script>
      <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
      <script type="text/javascript">stLight.options({onhover: false, publisher: "cda51e46-b98a-4edc-89fb-f3bc38b1817d", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
      <script>try{Typekit.load();}catch(e){}</script>';
}


// Show posts of 'post', 'page' and 'movie' post types on home page
add_action( 'pre_get_posts', 'add_jgiraldo_reportes_to_main_query' );

function add_jgiraldo_reportes_to_main_query( $query ) {
  if ( is_home() && $query->is_main_query() ){
    $query->set( 'post_type', array( 'post', 'jgiraldo_reportes' ) );
  }elseif(is_search() && $query->is_main_query()){
    $query->set( 'post_type', array( 'post', 'jgiraldo_reportes' ) );
  }elseif(is_tag() && $query->is_main_query()){
    $tag = explode(",", get_query_var("tag"));
    if(count($tag) > 1)
      $query->set("posts_per_page", -1);
  } 
  return $query;
}

function jgiraldo_ad_subscribe( $paged = 1, $rand = false ){
  global $post;
  
  echo '
  <!-- adModule section --> 
  <div class="admodule">
    <div id="mc_embed_signup" class="subscribe col-6">
      <form action="//jgiraldoreporta.us3.list-manage.com/subscribe/post?u=95d6e7a631cb8fee77c02bc0a&amp;id=08b261fe48" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <h1>Suscríbete y recibe nuestros reportes en tu correo</h1>
        <div style="text-align:center"><input class="item-text" type="email" name="EMAIL" id="mce-EMAIL" placeholder="nombre@correo.com"/><br></div>
        <div style="position: absolute; left: -5000px;"><input type="text" name="b_95d6e7a631cb8fee77c02bc0a_08b261fe48" tabindex="-1" value=""></div>
        <div style="text-align:center"> <input class="btn-ok" id="mc_embed_signup" type="submit" name="subscribe" value=""/></div>
      </form>
    </div>';

  
  if(!$rand)
    query_posts("post_type=ads_jgiraldo&posts_per_page=1&paged=" . $paged);
  else
    query_posts("post_type=ads_jgiraldo&posts_per_page=1&orderby=rand");
  if(have_posts()) : while(have_posts()) : the_post();
      echo '<div class="adsection col-8" style="background-color: '.get_post_meta( $post->ID, "back-color", TRUE ).'">';
      
      echo '<div class="image-adsection col-4">';
        the_post_thumbnail( "ads_image" , array("class" => "img-zapato") );
      echo '</div>
      <div class="adsdescription col-5">';
        the_content();
      echo '</div>';
      echo '</div>';
  endwhile;endif; wp_reset_query();
  
  echo '</div><!-- adModule section -->';
}

function jgiraldo_ad_poll( $paged = 1, $rand = false ){
 global $post; 
 
 echo '<!-- adModule section -->

  <div class="admodule2">';
  if(!$rand)
    query_posts("post_type=ads_jgiraldo&posts_per_page=1&paged=" . $paged);
  else
    query_posts("post_type=ads_jgiraldo&posts_per_page=1&orderby=rand");
  if(have_posts()) : while(have_posts()) : the_post();
    echo '<div class="addetails" style="background-color: '.get_post_meta( $post->ID, "back-color", TRUE ).'">
      <div class="image-adsection">'; 

        the_post_thumbnail( "ads_image" , array("class" => "img-zapato") );
    echo '</div>
      <div class="adsdescription">';
        the_content();
    echo '</div>
    </div>';
    endwhile;endif;wp_reset_query();
    echo '<div class="poll-section">' .
      do_shortcode("[poll id='".get_option("jgiraldo_week_poll")."']") .
    '</div>
  </div><!-- adModule section -->';

}

function jgiraldo_ads_loop($i, $ads_loop = false){
  global $ad_counter;
  $i++;
  if(is_home() || $ads_loop){
    if ((($i % 5) == 0) && $i != 0 ){
      if($ad_counter % 2 == 0){
        jgiraldo_ad_poll( $ad_counter, TRUE );
      }else{
        jgiraldo_ad_subscribe( $ad_counter, TRUE ); 
      }
      $ad_counter++;
    }
  }else{
    jgiraldo_ad_poll( 1, true );
  }
}

function jgiraldo_new_tags( $title = "Nuevo"){
  $tags = get_tags(array("orderby" => "", "order" => "DESC", "number" => 5 ));
  echo '<ul class="menu-side-nuevos">
          <h2>'.$title.': </h2>';
  foreach ($tags as $tag) {
    echo '
      <li class="filtro-item">
        <a href="'.get_bloginfo("url") ."/tag/". $tag->slug.'" title="'.$tag->name.'">
        <span>'.$tag->name.'</span>
        </a>
      </li>';        
  }        
          
  echo  '</ul>';
}

function jgiraldo_popular_tags( $title = "Popular"){
  $tags = get_tags(array("orderby" => "count", "order" => "DESC", "number" => 20 ));
  $all_tags = array();
  $i = 1;
  echo '<ul class="menu-side-popular">';
          if($title != "" ) echo '<h2>'.$title.': </h2>';
  echo '
      <li class="filtro-item">
        <a href="#">
        <span class="popular">Popular</span>
        </a>
      </li>';
  foreach ($tags as $tag) {
    if($i <= 9){
      echo '
        <li class="filtro-item">
          <a href="'.get_bloginfo("url") ."/tag/". $tag->slug.'" title="'.$tag->name.'">
          <span>'.$tag->name.'</span>
          </a>
        </li>';  
        $i++;
    }
      $all_tags[] = $tag->name;     
  }        
          
  echo  '<li class="filtro-item"><a href="'.get_bloginfo("url").'/?tag='.implode(",", $all_tags).'" title="Todas las Etiquetas"><span style="font-style:italic">Ver todo</span></a></li></ul>';
}

function jgiraldo_top_menu(){
  $args = array(
            'theme_location' => 'top_menu',
            'container_class' => 'links-items',
            'container_id' => '',
            'link_before' => '<h2>',
            'link_after' => '</h2>',
            'items_wrap' => '<ul>%3$s</ul><span class="social-icon-close second-icon-close"></span>',
            );
        
  wp_nav_menu( $args );
}

function jgiraldo_bottom_menu(){
  $args = array(
            "container" => "",
            'theme_location' => 'footer_menu',
            'container_id' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul class="menu-items col-5">%3$s<li class="item"><p>© 2016 JGiraldoReporta · Todos los derechos reservados</p></li></ul>',
            );
        
  wp_nav_menu( $args );
}

function jgiraldo_pagination(){
  global $wp_query;

  $big = 999999999; // need an unlikely integer

  echo "<div class='jgiraldo-pagination'>" . paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $wp_query->max_num_pages
  ) ) . "</div>";
}

function custom_search_where($where){
  global $wpdb;
  if (is_search())
    $where .= "OR (t.name LIKE '%".get_search_query()."%' AND {$wpdb->posts}.post_status = 'publish')";
  return $where;
}

function custom_search_join($join){
  global $wpdb;
  if (is_search())
    $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
  return $join;
}

function custom_search_groupby($groupby){
  global $wpdb;

  // we need to group on post ID
  $groupby_id = "{$wpdb->posts}.ID";
  if(!is_search() || strpos($groupby, $groupby_id) !== false) return $groupby;

  // groupby was empty, use ours
  if(!strlen(trim($groupby))) return $groupby_id;

  // wasn't empty, append ours
  return $groupby.", ".$groupby_id;
}

add_filter('posts_where','custom_search_where');
add_filter('posts_join', 'custom_search_join');
add_filter('posts_groupby', 'custom_search_groupby');


include_once("lib/theme-panel.php");


