<?php get_header() ?>

  
  <!-- Intro section -->
  <div class="intro-preposition">
    <div class="back-intro">
      <h1>
        <?php echo get_option("jgiraldo_headline")?><br>
        <a href="<?php echo get_option("jgiraldo_link")?>"><em>Leer más</em></a>
      </h1>
    </div>
  </div><!-- End intro section -->
  
  <?php  
    $i = 0;
    global $ad_counter;
    $ad_counter = 1; 
    if(have_posts()) : while (have_posts()) : the_post(); ?> 
  <?php jgiraldo_main_loop( $i ); 
        $i++;

    endwhile;endif;
    ?>
    <?php jgiraldo_pagination(); ?>
    <?php jgiraldo_tag_cloud() ?>
    

<?php get_footer() ?>