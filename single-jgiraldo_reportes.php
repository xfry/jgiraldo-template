<?php get_header() ?>
  
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
  <div class="reporte-unico">
    <!-- heder reporte-unico -->
    <div class="reporte-unico-container">
      <div class="header-reporte-unico">
        <h1 class="titulo"><?php the_title() ?></h1>
        <div class="social-icons-hr"></div>
      </div><!-- end header reporte -->

      <div class="title-report">
        <?php the_content() ?>
      </div>
      
      <!-- report social section -->
      <div class="social">
        <div class="social-icons-hr"></div>
        <?php jgiraldo_social_icons() ?>
      </div><!-- end report social -->
    </div><!-- end report container -->
  </div><!-- end reporte -->
  
  <?php jgiraldo_loop_reportes() ?>
<?php endwhile;endif; ?>
 <?php //jgiraldo_ad_subscribe() ?> 

  <?php jgiraldo_tag_cloud()?>
<?php get_footer() ?>