<?php get_header() ?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
  <div class="reporte-unico">
    <!-- heder reporte-unico -->
    <div class="reporte-unico-container">
      <div class="header-reporte-unico">
        <h1 class="titulo"><?php the_title() ?></h1>
        <div class="social-icons-hr"></div>
      </div><!-- end header reporte -->

      <div class="title-report">
        <?php echo get_post_meta(get_the_ID(),"intro_historias",true) ?>
      </div>
      
      <!-- report social section -->
      <div class="social">
        <div class="social-icons-hr"></div>
        <?php jgiraldo_social_icons() ?>
      </div><!-- end report social -->
    </div><!-- end report container -->
  </div><!-- end reporte -->
  
  <div class="page-container col-14"> 
    <?php the_content() ?>
  </div>
  <?php if(get_post_meta(get_the_ID(),"outro_historias",true) != "") : ?>
  <div class="reporte-unico-2">
    <!-- heder reporte-unico -->
    <div class="reporte-unico-container">
      <div class="title-report">
        <?php echo get_post_meta(get_the_ID(),"outro_historias",true) ?>
      </div>
       
      <!-- report social section -->
      <div class="social">
        <div class="social-icons-hr"></div>
        
        <?php jgiraldo_post_tags() ?>

        <div class="social-icons-hr"></div>
        <?php jgiraldo_social_icons() ?>
      </div><!-- end report social -->
    </div><!-- end report container -->
  </div><!-- end reporte -->
<?php endif; ?>
<?php endwhile;endif; ?>
  <?php jgiraldo_historias_related() ?>

  <div class="search">
    <?php jgiraldo_related_posts_by_tag( TRUE ) ?>
  </div>

  <?php jgiraldo_tag_cloud() ?>
<?php get_footer() ?>