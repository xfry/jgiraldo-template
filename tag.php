<?php get_header()?>


  <div class="cloud-tags-header">
    <div class="cloud-tags">
      <h3>Estos contenidos tienen que ver con</h3>
      <?php 
      $tag = explode(",", get_query_var("tag"));
      ?>
      <?php if(count($tag) > 1) : ?>
        <h2>Todos</h2>
      <?php else: ?>
        <h2><?php single_cat_title( ); ?></h2>
      <?php endif; ?>
        
      <?php jgiraldo_tag_cloud(5, "También te pueden interesar otras cosas como", false) ?>
    </div><!-- end cloudtags -->
  </div>
  
  <?php jgiraldo_historias_related()
   ?>  

  <!-- Reporte 01 section -->
  <div class="reporte">
    <div class="reporte-wrap-serach">
      
      <?php jgiraldo_tag_search_cat_loop() ?>
      
    </div>
  </div><!-- end reporte -->

  <?php jgiraldo_ad_poll( 1, true) ?>
  <?php jgiraldo_pagination(); ?>
  <?php jgiraldo_tag_cloud() ?>
<?php get_footer() ?>