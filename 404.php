<?php get_header() ?>
  <div class="reporte-unico">
    <!-- heder reporte-unico -->
    <div class="reporte-unico-container">
      <div class="header-reporte-unico">
        <h1 class="titulo">Papel no encontrado</h1>
        <div class="social-icons-hr"></div>
      </div><!-- end header reporte -->
      <!-- report social section -->
      
    </div><!-- end report container -->
  </div><!-- end reporte -->
  
  <div class="page-container col-14"> 
      <h2> Oops parece que no existe el papel que estas buscando.</h2>
  </div>
<?php get_footer() ?>