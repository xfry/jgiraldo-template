
  <!-- Panel 1 -->
  <div class="panel1" id="post-<?php the_ID()?>">  
    <div class="panel1-left-side col-10" style="float:<?php echo $float?>">
      <!-- header reporte -->
      <div class="header-reporte">
        <h1 class="titulo"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
        <?php jgiraldo_post_tags(); ?>
        
      </div><!-- end header reporte -->

      <div class="panel1-left-container">
        <div class="icon-info-container"><span class="info-icon"></span></div>
        <?php 
          if(get_post_meta(get_the_ID(), "thinglink",true) != "" )
            echo get_post_meta(get_the_ID(), "thinglink",true);
          else  
            the_post_thumbnail("full");
        ?>
      </div>

      <!-- report social section -->
      <div class="social2">
        <?php jgiraldo_social_icons() ?>
        <div class="social-icons-hr"></div>
      </div><!-- end report social -->
    </div>
    
    <!-- Over notification on click -->
    <div class="adsdtext">
      <div class="icon-close">
        <img src="<?php echo get_option("jgiraldo_close_button")?>"  alt="logo information">
      </div>
      <div class="adstext-description">
        <?php the_content() ?>
      </div>
      <!-- report social section -->
      <div class="social2">
        <?php jgiraldo_social_icons() ?>
      </div><!-- end report social -->
      <!-- over notification on click -->
    </div>

    <div class="panel1-right-side col-4 <?php echo $float_left?>">
      <div class="panel1-right-container">
        <h1 class="titulo"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
          <?php the_content();
            jgiraldo_post_tags();
          ?>
          <?php jgiraldo_social_icons() ?>
      </div>
    </div>
  </div><!-- end panel1 -->