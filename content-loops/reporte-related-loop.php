
<?php 
global $report_permalink, $post; 

if ((get_post_meta($post->ID,"imagen_reporte",true) ) != '') {
    $attachment_id = get_post_meta($post->ID,"imagen_reporte",true);
    $report_image = wp_get_attachment_image_src( $attachment_id, "report_image");
    $report_image_2 = wp_get_attachment_image_src( $attachment_id, "report_image_2");
}     
if (($iteration == 0) || ($iteration % 4 == 0) ) : ?>
		<div class="report-category">
<?php endif; ?>
<?php if($even && $container) : ?>	
		<!-- container true  even true -->
            <div class="mix <?php jgiraldo_post_attributes() ?> category-card impar col-4">
              <div class="container-img-reporte"><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>">
                <?php if(get_post_meta($post->ID,"imagen_reporte",true) != '') :?>
                        <img src="<?php echo $report_image_2[0] ?>" alt="<?php the_title()?>" />
                  <?php else : 
                          the_post_thumbnail("report_image_2" ); 
                        endif;
                  ?></a>
              </div><div class="detail-section">
                <h2><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>"><?php the_title() ?></a></h2>
              </div>
              <div class="img-icon-par"></div>
            </div>
<?php elseif(!$even && $container) :?>
            <!-- container true  even false -->
            <div class="mix <?php jgiraldo_post_attributes() ?> category-card par col-3">
              <div class="container-img-reporte"><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>">
                <?php if(get_post_meta($post->ID,"imagen_reporte",true) != '') :?>
                    <img src="<?php echo $report_image[0] ?>" alt="<?php the_title()?>" />
                  <?php else : 
                          the_post_thumbnail("report_image" ); 
                        endif;
                  ?></a>
              </div><div class="detail-section">
                <h2><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>"><?php the_title() ?></a></h2>
              </div>
              <div class="img-icon-par"></div>
            </div>
        
<?php elseif($even && !$container) :  ?>
		<!-- container false  even true -->
            <div class="mix <?php jgiraldo_post_attributes() ?> category-card par col-3">
              <div class="container-img-reporte"><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>">
                  <?php if(get_post_meta($post->ID,"imagen_reporte",true) != '') :?>
                          <img src="<?php echo $report_image[0] ?>" alt="<?php the_title()?>" />
                  <?php else : 
                          the_post_thumbnail("report_image" ); 
                        endif;
                  ?></a>
              </div><div class="detail-section">
                <h2><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>"><?php the_title() ?></a></h2>
              </div>
              <div class="img-icon-par"></div>
            </div>
<?php elseif(!$even && !$container) :  ?>
            <!-- container false  even false -->
            <div class="mix <?php jgiraldo_post_attributes() ?> category-card impar col-4">
              <div class="container-img-reporte"><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>">
                <?php if(get_post_meta($post->ID,"imagen_reporte",true) != '') :?>
                    <img src="<?php echo $report_image_2[0] ?>" alt="<?php the_title()?>" />
                  <?php else : 
                          the_post_thumbnail("report_image_2" ); 
                        endif;
                  ?></a>
              </div><div class="detail-section">
                <h2><a href="<?php echo $report_permalink ?>#post-<?php the_ID()?>" title="<?php the_title()?>"><?php the_title() ?></a></h2>
              </div>
              <div class="img-icon-par"></div>
            </div>
<?php endif; ?>
<?php if ( ( ($iteration != 0) && ($iteration % 4 == 3)  ) || 
         ($iteration == ($total_posts - 1)) )  : ?>
		</div><!-- end report category -->
	

<?php endif ?>