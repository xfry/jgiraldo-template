<?php get_header() ?>

<?php if(have_posts()): while(have_posts())  : the_post(); ?>
  <!-- Panel 1 -->
  <div class="panel1">
    <div class="papel-panel">
    <div class="panel1-left-side col-10 papel-panel">
      <!-- header reporte -->
      <div class="header-reporte">
        <h1 class="titulo"><?php the_title() ?></h1> 
        <?php jgiraldo_post_tags(); ?>
      </div><!-- end header reporte -->

      <div class="panel1-left-container">
        <div class="icon-info-container"><span class="info-icon"></span></div>
        <?php 
          if(get_post_meta(get_the_ID(), "thinglink",true) != "" )
            echo get_post_meta(get_the_ID(), "thinglink",true);
          else  
            the_post_thumbnail("full"); ?>
      </div>

      <!-- report social section -->
      <div class="social2">
       <?php jgiraldo_social_icons() ?>
      </div><!-- end report social -->
    </div>
    <!-- Over notification on click -->
    <div class="adsdtext">
      <div class="icon-close">
        <img src="<?php echo get_option("jgiraldo_close_button")?>"  alt="logo information">
      </div>
      <div class="adstext-description">
        <?php the_content() ?>
      </div>
      <!-- report social section -->
      <div class="social2">
        <?php jgiraldo_social_icons() ?>
      </div><!-- end report social -->
      <!-- over notification on click -->
    </div>
    <div class="panel1-right-side right">
      <div class="panel1-right-container">
        <h1 class="titulo"><?php the_title() ?></h1>
        <?php the_content() ?>
          <?php jgiraldo_post_tags(); ?>
          <?php jgiraldo_social_icons() ?>
      </div>
    </div>
    </div>
  </div><!-- End panel1 -->
<?php endwhile;endif; ?>
  <!-- Image credits and buttons -->
  <div class="side-menu">
    <div class="image-credits">
      <p class="credits"><?php echo get_post_meta($post->ID, "img_credit",true) ?></p>
      <div class="side-menu-conten2">
        <a href="<?php jgiraldo_get_prev_post_link()?>" class="menu-content-left">
          <span class="left-icon"></span>
          <span class="text-left-icon"><h2>Anterior</h2></span>
        </a>
        
        <?php jgiraldo_social_icons() ?>

        <a href="<?php jgiraldo_get_next_post_link()?>" class="menu-content-right">
          <span class="text-right-icon"><h2>Siguiente</h2></span>
          <span class="right-icon"></span>
        </a>
      </div>
    </div>

    <div class="side-menu-conten col-4">
        <a href="<?php jgiraldo_get_prev_post_link()?>" class="menu-content-left">
          <span class="left-icon"></span>
          <span class="text-left-icon"><h2>Anterior</h2></span>
        </a>

        <a href="<?php jgiraldo_get_next_post_link()?>" class="menu-content-right">
          <span class="text-right-icon"><h2>Siguiente</h2></span>
          <span class="right-icon"></span>
        </a>
    </div>

    <div class="side-menu-mobile"></div>
  </div><!-- end side menu -->
  
  <!-- Reporte 01 section -->
  <div class="reporte">
    <div class="reporte-wrap">
      <!-- heder reporte -->
      <?php jgiraldo_related_posts_by_tag() ?>
      
    </div>
  </div><!-- end reporte -->

 <?php jgiraldo_ad_subscribe( 1, true )?>

<?php get_footer() ?>